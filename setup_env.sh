#!/bin/sh

echo plugins=$PLUGIN >> conf/kong.conf
echo admin_listen=$ADMIN_LISTEN >> conf/kong.conf
echo proxy_listen=$PROXY_LISTEN >> conf/kong.conf
echo database=$DATABASE >> conf/kong.conf
echo pg_timeout=$PG_TIMEOUT >> conf/kong.conf
echo pg_host=$PG_HOST >> conf/kong.conf
echo pg_port=$PG_PORT >> conf/kong.conf
echo pg_username=$PG_USERNAME >> conf/kong.conf
echo pg_password=$PG_PASSWORD >> conf/kong.conf
echo pg_database=$PG_DATABASE >> conf/kong.conf
echo log_level=$LOG_LEVEL >> conf/kong.conf

cp conf/kong.conf /etc/kong/

kong start