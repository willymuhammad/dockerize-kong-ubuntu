FROM ubuntu:18.04

RUN apt update

RUN DEBIAN_FRONTEND=noninteractive TZ=Asia/Jakarta apt install -y git wget zlib1g-dev nginx
RUN mkdir /tmp/kong-cognisia
WORKDIR /tmp/kong-cognisia

RUN git clone https://github.com/willymuhammad/spada-kong.git
COPY . .

RUN ls deb/
RUN dpkg -i deb/kong_2.4.0_amd64.deb
RUN luarocks install spada-kong/spada-kong-1.4-0.rockspec
RUN cat conf/kong.conf
RUN cp conf/kong.conf /etc/kong/
RUN cp conf/sysctl.conf /etc/
RUN cp conf/limits.conf /etc/security/
RUN sysctl -p
RUN ulimit -n

EXPOSE 8000
CMD ["nginx", "-g", "daemon off;"]